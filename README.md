# Otus.Teaching.PromoCodeFactory

Проект для домашних заданий и демо по курсу `C# ASP.NET Core Разработчик` от `Отус`.
Cистема `Promocode Factory` для выдачи промокодов партнеров для клиентов по группам предпочтений.

Данный проект является стартовой точкой для Homework №3

Подробное описание проекта можно найти в [Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Home)

Описание домашнего задания в [Homework Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-3)


## Описание/Пошаговая инструкция выполнения домашнего задания:   

1. Добавляем docker-compose файл, где конфигурируем PostgreSQL БД.    
2. Добавляем начальную миграцию БД    
3. Вместо SQLite добавляем PostgreSQL в приложение и настраиваем EF на работу с PostgreSQL, проверяем через Swagger, что Read/Create/Update методы работают    
4. Настраиваем сборку на Gitlab. Упаковка в Docker образ и передача в хранилище GitLab, что позволит легко развернуть приложение в любом окружении.    
